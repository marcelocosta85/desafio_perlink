﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DesafioPerlinkAPI.Data;
using DesafioPerlinkAPI.Models;

namespace DesafioPerlinkAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessosController : ControllerBase
    {
        private readonly ProcessoContext _context;

        public ProcessosController(ProcessoContext context)
        {
            _context = context;
        }

        // GET: api/Processos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Processo>>> GetProcessos()
        {
            return await _context.Processos.ToListAsync();
        }

        // GET: api/Processos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Processo>> GetProcesso(string id)
        {
            var processo = await _context.Processos.FindAsync(id);

            if (processo == null)
            {
                return NotFound();
            }

            return processo;
        }

        // PUT: api/Processos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProcesso(string id, Processo processo)
        {
            if (id != processo.numeroDoProcesso)
            {
                return BadRequest();
            }

            _context.Entry(processo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProcessoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Processos
        [HttpPost]
        public async Task<ActionResult<Processo>> PostProcesso(Processo processo)
        {
            
            if (!ProcessoExists(processo.numeroDoProcesso))
            {
                _context.Processos.Add(processo);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetProcesso", new { id = processo.numeroDoProcesso }, processo);
            }
            else
            {
                return BadRequest("Este processo já foi cadastrado!");
            }
            

        }

        // DELETE: api/Processos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Processo>> DeleteProcesso(string id)
        {
            var processo = await _context.Processos.FindAsync(id);
            if (processo == null)
            {
                return NotFound();
            }

            _context.Processos.Remove(processo);
            await _context.SaveChangesAsync();

            return processo;
        }

        private bool ProcessoExists(string id)
        {
            return _context.Processos.Any(e => e.numeroDoProcesso == id);
        }
    }
}
