﻿using DesafioPerlinkAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace DesafioPerlinkAPI.Data
{
    public class ProcessoContext : DbContext
    {
        public ProcessoContext(DbContextOptions<ProcessoContext> options) : base(options)
        {

        }

        public DbSet<Processo> Processos { get; set; }
        
}
}
