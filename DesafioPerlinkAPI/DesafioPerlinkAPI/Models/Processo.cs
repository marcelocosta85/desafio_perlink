﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DesafioPerlinkAPI.Models
{
    public class Processo
    {
        [Key]
        public string numeroDoProcesso { get; set; }
        [Required]
        public DateTime dataDeCriacaoDoProcesso { get; set; }
        [Required]
        public decimal valor { get; set; }
        [Required]
        public string escritorio { get; set; }

        public Processo(string numeroDoProcesso, DateTime dataDeCriacaoDoProcesso, decimal valor, string escritorio)
        {
            this.numeroDoProcesso = numeroDoProcesso;
            this.dataDeCriacaoDoProcesso = dataDeCriacaoDoProcesso;
            this.valor = valor;
            this.escritorio = escritorio;
        }
    }
}
