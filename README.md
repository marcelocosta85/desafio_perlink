# Desafio para vaga de desenvolvedor da Perlink

## Considerações Gerais

## Sobre a API

O projeto foi desenvolvido com .NetCore API 2.2 e com EntityFramework 2.2.6 para persistência dos dados.

Por motivos de falta de tempo, infelizmente criei somente uma API muito simplória, fazendo uma simulação de banco de dados usando Microsoft.EntityFrameworkCore.InMemory. Ela recebe o JSON e o armazena usando o próprio campo "numeroDoProcesso" como Id de maneira que não poderá ser armazenado um processo com o mesmo número. Caso aconteça uma mensagem de erro será retornada.
